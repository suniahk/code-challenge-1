import { ActionTypes } from "../constants";

export const updateLockStatus = ( lockState, deviceId ) => ( {
    type: ActionTypes.UPDATE_LOCK_STATUS,
    id: deviceId,
    state: lockState
} );

export const addDevice = deviceData => ( {
    type: ActionTypes.ADD_DEVICE,
    data: deviceData
} );

export const addUser = userData => ( {
    type: ActionTypes.ADD_USER,
    data: userData
} );