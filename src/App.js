import React from 'react';
import './App.css';
import TabBar from './components/TabBar';

function App() {
  return (
    <div className="App">
      <TabBar />
    </div>
  );
}

export default App;
