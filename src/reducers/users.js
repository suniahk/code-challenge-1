import { ActionTypes } from '../constants';

const users = ( state = [], action ) => {
    switch( action.type ) {
        case ActionTypes.ADD_USER:
            return [ ...state, action.data ];

        case ActionTypes.REMOVE_USER:
            return state.filter( id => { return id !== action.data.id; } )

        default:
            return state;
    }
}

export default users;