import { ActionTypes } from '../constants';

const devices = ( state = [], action ) => {
    switch( action.type ) {
        case ActionTypes.ADD_DEVICE:
            return [ ...state, action.data ];

        case ActionTypes.REMOVE_DEVICE:
            return state.filter( id => { return id !== action.data.id; } )

        case ActionTypes.UPDATE_LOCK_STATUS:
            return state.map( element => {
                if( element.id === action.id ) {
                    if( element.attributes.state === "locked" ) {
                        element.attributes.state = "unlocked"
                    } else if( element.attributes.state === "unlocked" ) {
                        element.attributes.state = "locked"
                    }
                }

                return element;
            } )

        default:
            return state;
    }
}

export default devices;