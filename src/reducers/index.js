import { combineReducers } from 'redux';
import devices from './devices';
import users from './users';

export default combineReducers( {
    devices,
    users
} );