export const UserType = {
    GUEST : "access_guest",
    USER : "access_user"
}

export const ActionTypes = {
    ADD_DEVICE: "ADD_DEVICE",
    REMOVE_DEVICE: "REMOVE_DEVICE",
    ADD_USER: "ADD_USER",
    REMOVE_USER: "REMOVE_USER",
    UPDATE_LOCK_STATUS: "UPDATE_LOCK_STATUS"
}