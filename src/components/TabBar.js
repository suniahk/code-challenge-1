import React from 'react';
import { Tabs } from 'antd';
import DevicePane from './panes/DevicePane'
import UserPane from './panes/UserPane'
const { TabPane } = Tabs;

const TabBar = ( props ) => (
    <Tabs defaultActiveKey="1">
        <TabPane tab="Devices" key="1">
            <DevicePane />
        </TabPane>
        <TabPane tab="Users" key="2">
            <UserPane />
        </TabPane>
    </Tabs>
)

export default TabBar;