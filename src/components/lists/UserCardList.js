import React from 'react';
import UserCard from '../cards/UserCard';
import { connect } from 'react-redux';

// NOTE: Data has 4 users but both guests are expired.
const UserCardList = ( { users } ) => (
    <div>
        { users.filter( user => user.attributes.status === "current" && (!user.attributes.ends_at || Date.now() < new Date( user.attributes.ends_at ) ) ).map( user => (
            <UserCard
                key={ user.id }
                name={ user.attributes.name }
                subHeader={ user.attributes.email || user.attributes.phone }
                id={ user.id }
                userType={ user.type }
                startDate={ user.attributes.starts_at }
                endDate={ user.attributes.ends_at }
            />
        ) ) }
    </div>
)

const mapStateToProps = state => ({
    users: state.users
  })

export default connect( mapStateToProps )( UserCardList );