import React from 'react';
import LockCard from '../cards/LockCard';
import ThermostatCard from '../cards/ThermostatCard';
import { connect } from 'react-redux';

const DeviceCardList = ( { devices } ) => (
    <div>
        { devices.map( device => (
            <>
                { device.type === "lock" && <LockCard
                    key={ device.id }
                    name={ device.attributes.name }
                    subHeader={ device.attributes.model_number }
                    lockState={ device.attributes.state } id={ device.id }
                /> }

                { device.type === "thermostat" && <ThermostatCard
                    key={ device.id }
                    name={ device.attributes.name }
                    subHeader={ device.attributes.model_number }
                    temp={ device.attributes.temperature }
                    targetTemp={ device.attributes.target_temperature }
                    tempUnit={ device.attributes.unit }
                /> }
            </>
        ) ) }
    </div>
)

const mapStateToProps = state => ({
    devices: state.devices
  })

export default connect( mapStateToProps )( DeviceCardList );