import React from 'react';
import { Tag } from 'antd';
import { UserType } from '../constants';

const UserTag = ( { userType, startDate, endDate } ) => {
    let tagLabel = "";
    let tagType = "";

    if( userType === UserType.USER || ( userType === UserType.GUEST && !!startDate && !!endDate && Date.now() > new Date(startDate) && Date.now() < new Date(endDate) ) ) {
        tagLabel = "ACTIVE";
        tagType = "activeTag";
    } else if( userType === UserType.GUEST && !!startDate && Date.now() < new Date(startDate) ) {
        tagLabel = "UPCOMING";
        tagType = "upcomingTag";
    } else {
        return null;
    }

    if( !!userType ) {
        return (
            <Tag className={ tagType }>{ tagLabel }</Tag>
        );
    }

    return "";
}

export default UserTag;