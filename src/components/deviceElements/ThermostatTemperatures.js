import React from 'react';
import TemperatureDisplay from '../TemperatureDisplay';

const ThermostatTemperatures = ( { temp, targetTemp, tempUnit } ) => (
    <>
        <TemperatureDisplay temp={ temp } tempType="current" tempUnit={ tempUnit } />
        <i className="fa fa-long-arrow-right temperatureArrow" aria-hidden="true"></i>
        <TemperatureDisplay temp={ targetTemp } tempType="target" tempUnit={ tempUnit } />
    </>
)

export default ThermostatTemperatures;