import React from 'react';

const TemperatureDisplay = ( { temp, tempType, tempUnit } ) => (
    <div className="temperatureDisplay">
        <div><span className="thermostatTemp">{ temp }</span> <span className="thermostatUnit">&deg;{ tempUnit }</span></div>
        <div>
            { tempType === "current" && <label>CURRENT</label> }
            { tempType === "target" && <label>TARGET</label> }
        </div>
    </div>
)

export default TemperatureDisplay;