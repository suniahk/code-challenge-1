import React from 'react';
import BasicCard from './BasicCard';
import { connect } from 'react-redux';
import ThermostatTemperatures from '../deviceElements/ThermostatTemperatures';

const ThermostatCard = ( { dispatch, ...props } ) =>  (
    <BasicCard
        header={ props.name }
        subHeader={ props.subHeader }
        footer={ <ThermostatTemperatures temp={ props.temp } targetTemp={ props.targetTemp } tempUnit={ props.tempUnit } /> } />
)

export default connect()(ThermostatCard);