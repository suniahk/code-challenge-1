import React from 'react';
import BasicCard from './BasicCard';
import { dateRange } from '../../utils';
import UserTag from '../UserTag';

const UserCard = ( props ) => (
    <BasicCard
        header={ props.name }
        subHeader={ props.subHeader }
        content={ dateRange( props.startDate, props.endDate ) }
        footer={ <UserTag userType={ props.userType } startDate={ props.startDate } endDate={ props.endDate } /> }
    />
)

export default UserCard;