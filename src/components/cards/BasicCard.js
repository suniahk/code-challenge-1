import React from 'react';
import { Avatar, Card } from 'antd';

const BasicCard = ( props ) => (
    <Card style={{ width: 300 }} className="basic-card">
        <Avatar size={ 96 } icon="user" />
        <div className="card-contents">
            <div>
                <h2>{ props.header }</h2>
                { !!props.subHeader && <h3>{ props.subHeader }</h3> }
                { !!props.content && <p>{ props.content }</p> }
            </div>
            { !!props.footer && <div className="card-footer">{ props.footer }</div> }
        </div>
    </Card>
)

export default BasicCard;