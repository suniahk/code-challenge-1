import React from 'react';
import BasicCard from './BasicCard';
import LockStatus from '../LockStatus';
import { connect } from 'react-redux';
import { updateLockStatus } from '../../actions';

const LockCard = ( { dispatch, ...props } ) =>  (
    <BasicCard
        header={ props.name }
        subHeader={ props.subHeader }
        footer={ <LockStatus isLocked={ props.lockState }
        onChange={ switchValue => { dispatch( updateLockStatus( switchValue, props.id ) ) } }
    /> } />
)

export default connect()(LockCard);