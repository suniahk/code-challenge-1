import React from 'react';
import { connect } from 'react-redux';
import DeviceCardList from '../lists/DeviceCardList';
import { fetchData } from '../../utils'
import { addDevice } from '../../actions';

class DevicePane extends React.Component {
    async componentDidMount() {
        const { dispatch } = this.props;
        let deviceData = await fetchData( `https://gist.githubusercontent.com/rafaelsales/d924d4f52c925d1a1c969e4fd448dc5a/raw/735da50552dc49b1229068fa5e0de0c447d01628/z_devices.json` );

        deviceData.data.map( element => dispatch( addDevice( element ) ) );
    }

    render() {
        return (
            <DeviceCardList />
        );
    }
}

export default connect()( DevicePane )