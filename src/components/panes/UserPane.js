import React from 'react';
import { connect } from 'react-redux';
import UserCardList from '../lists/UserCardList';
import { fetchData } from '../../utils'
import { addUser } from '../../actions';

class UserPane extends React.Component {
    async componentDidMount() {
        const { dispatch } = this.props;
        let userData = await fetchData( `https://gist.githubusercontent.com/rafaelsales/07536fe2382cac28326989bca7535c02/raw/1218ebef349d03f721f8226a3f075cc771c61dbd/z_users.json` );

        userData.data.map( element => dispatch( addUser( element ) ) );
    }

    render() {
        return (
            <UserCardList />
        );
    }
}

export default connect()( UserPane )