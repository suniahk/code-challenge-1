import React from 'react';
import { Switch } from 'antd';

const LockStatus = ( props ) => {
    const lockState = props.isLocked === "locked" ? true : false;
    return (
        <div className="lock-state">
            <Switch defaultChecked={ lockState } onChange={ props.onChange } className={ props.isLocked } />
            <span className="lock-state-text">
                { lockState && 
                    <>
                        <i className="fa fa-lock"></i> 
                        Locked
                    </> }
                { !lockState && 
                <>
                    <i className="fa fa-unlock-alt"></i> 
                    Unlocked
                </> }
            </span>
        </div>
    );
}

export default LockStatus;