import dayjs from 'dayjs';

export const fetchData = async ( url ) => {
    let response = await fetch( url );
    let data = await response.json()

    return data;
};

export const dateRange = ( startDate, endDate ) => {
    if( startDate == null ) {
        return null;
    }

    const start = dayjs( startDate ).format( ('MMM DD hh:mm') );
    const end = dayjs( endDate ).format( ('MMM DD hh:mm') );

    return `${start} - ${end}`;
}