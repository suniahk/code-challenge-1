# About this project

The codebase here was created in response to a code challenge from a prospective employer.  The idea was to create a design based off of a screenshot using React.  

# Technologies Used
- React
- Redux